#!/bin/env python3

import time
import threading
import argparse
import urllib3
import sys
import random
import datetime
import re
import os

example_text = '''example:

# verbose shows url with returned status code
python cpload.py --verbose --touri "http://127.0.0.1:8080" --ratemin 360 --ratemax 7200 --ramptime 0.1 --duration 0.2

# functional test
python cpload.py --urlfile logs.txt --fromuri base.uri --touri "https://tst.doesnotexist.org" --ratemin 360 --ratemax 7200 --ramptime 1 --duration 2

# stress test (real world rampup)
python cpload.py --urlfile logs.txt --fromuri base.uri --touri "https://tst.doesnotexist.org" --ratemin 360 --ratemax 7200 --ramptime 0.5  --duration 0.5 ;
python cpload.py --urlfile logs.txt --fromuri base.uri --touri "https://tst.doesnotexist.org" --ratemin 7200 --ratemax 90000 --ramptime 7 --duration 10

# Worst case
python cpload.py --urlfile logs.txt --fromuri base.uri --touri "https://tst.doesnotexist.org" --ratemin 360 --ratemax 7200 --ramptime 0.5  --duration 0.5 ;
python cpload.py --urlfile logs.txt --fromuri base.uri --touri "https://tst.doesnotexist.org" --ratemin 7200 --ratemax 120000 --ramptime 6 --duration 10
'''

parser = argparse.ArgumentParser(description='http log replay', epilog=example_text, formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('--urlfile', dest='urlfile', default='logs.txt',
                    help='the cleaned up http log')
parser.add_argument('--fromuri', dest='fromuri', default='base.uri',
                    help='base uri/hostname as in the log file, this is used to edit it to the destination uri.')
parser.add_argument('--touri', dest='touri', default='https://tst.doesnotexist.org',
                    help='the uri/hostname you want to send the traffic to')
parser.add_argument('--ratemin', dest='ratemin', default='360', type=int,
                    help='minimum req/hour')
parser.add_argument('--ratemax', dest='ratemax', default='7200', type=int,
                    help='maximum req/hour')
parser.add_argument('--ramptime', dest='ramptime', default='1', type=float,
                    help='ramp up time in hours')
parser.add_argument('--duration', dest='duration', default='1', type=float,
                    help='total test duration in hours')
parser.add_argument('--verbose', dest='verbose', action='store_true',
                    help='Output all request results')
parser.add_argument('--filters', dest='filters',
                    help='the uri filters and rewrites file')
parser.set_defaults(verbose=False)
args = parser.parse_args()

if len(sys.argv) < 2:
    parser.print_help()
    sys.exit(0)

# rampup calc
starttime = int(time.time())
ramptimes = args.ramptime*3600
duration = args.duration*3600
rampendtime = starttime+ramptimes
endtime = starttime + duration
ramptotalincrease = args.ratemax - args.ratemin
rampincreasepersec = ramptotalincrease / ramptimes
ratePerHour = args.ratemin
lastUpdateTime = 0

threads = []
urllib3.disable_warnings()

http_pool = urllib3.PoolManager(num_pools=10, maxsize=10000, cert_reqs='CERT_NONE', timeout=120)

def send_request(http_pool, url):
    """Send the get request"""
    currentRate = round(ratePerHour)
    try:
        r = http_pool.request('GET', url)
        if args.verbose:
            print(str(r.status), url)
        if r.status > 499:
            print("you broke it at:", str(currentRate), "req/hour")
            os._exit(1)
    except:
        print("you broke it at:", str(currentRate), "req/hour")
        os._exit(1)

# pull in the url filter rewriter functions
if args.filters:
    with open(args.filters) as f:
        contents = f.read()
        code_obj = compile(contents, args.filters, 'exec')
        exec(code_obj)
else:
    filters = {'': send_request}

with open(args.urlfile) as f:
    lines = f.read().splitlines()
    while True:
        for uri in lines:
            now = int(time.time())
            if ( now > endtime ):
                sys.exit()
            if ( (endtime - now) % 10 ) < 1:
                if lastUpdateTime != endtime - now:
                    lastUpdateTime = endtime - now
                    print("Load test running at:", str(round(ratePerHour)), "req/hour")
            try:
                completed = False
                for filtertext in filters:
                    if (filtertext in uri ) and not completed:
                        uri = uri.replace(args.fromuri, args.touri)
                        t = threading.Thread(target=filters[filtertext], args=[http_pool, uri])
                        t.daemon = True
                        threads.append(t)
                        t.start()
                        completed = True

                if ( now < rampendtime ):
                    runtime = ((now - starttime) + 1 )
                    increment = (runtime * rampincreasepersec)
                    ratePerHour = args.ratemin + increment
                    sleeptime = (3600/ratePerHour)
                time.sleep(sleeptime)
            except KeyboardInterrupt:
                print("\nStopping load test at:", str(round(ratePerHour)), "req/hour \n")
                sys.exit()

