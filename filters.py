print("loaded filters:")
# this file is included in the main cpload.py to add special sauce to your log replay.

# Here's the base function, what you do inside it is up to you.
#
# def uri_editor(http_pool, url):
#     send_request(http_pool, url)
#

# Here we select a random product from the magic list instead of using the one from the log.
# This might be helpfull when you have a small subset of the products in you tst / acc / mock environment
magicList = ['magic1', 'product2', 'destination3', 'package4']
def magic_edit(http_pool, url):
    """Select only magic products"""
    magic = random.choice(magicList)
    url = str(args.touri) + "/api/v1/add/controlled/randomness/" + magic + "/test"
    send_request(http_pool, url)

# In this example we replace a uri that is date sensitive with an url that always has a date in the future.
today = datetime.date.today()
def future(http_pool, url):
    """Always use future dates"""
    days = datetime.timedelta(days=random.randint(3, 20))
    date = today + days
    url = str(args.touri) + "/api/v1/date-sensitive/" + date.strftime("%Y%m%d") + "/test"
    send_request(http_pool, url)

# ALL CAPS FOR ADDED DRAMA
def to_upper(http_pool, url):
    """URI TO UPPER"""
    send_request(http_pool, str(url).upper())

# This is how the uri gets mapped to the correct editor function.
# It's selected based on if a string is string in uri, the first match will be used.
filters = {
    '/magic/': magic_edit,
    'api/v1/date': future,
    'web': to_upper,
    '': send_request
}

# draw a pretty overview of the loaded filters
from tabulate import tabulate
table = [["function", "help text", "uri filter"]]
for filter in filters:
    table.append([filters[filter].__name__, filters[filter].__doc__, filter])
print("\n", tabulate(table,headers="firstrow"), "\n")