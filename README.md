## CPload

This tool can use access logs from the to simulate a gradual traffic rampup, this is usefull in scaling tests and validating performance requirements.

[![asciicast](https://asciinema.org/a/305069.svg)](https://asciinema.org/a/305069?autoplay=true&size=21)

The script takes in a file with a single url per line starting with a prefix that you can edit to send the traffic to any location you like.

Example log lines:

```txt
 #  cat logs.txt
base.uri/
base.uri/web/location/
base.uri/api/v1/location/?test=true
base.uri/web/location/nested/pages
base.uri/api/v1/date-sensitive/20200213/test
base.uri/
base.uri/api/v1/add/controlled/randomness/magic/test
base.uri/
```

## How to use

- *Clone the repo*
- *Install requirements with* `pip install --user -r requirements.txt`

Check the help included in the script, it is pretty self explanatory.

```txt
   python cpload.py --help
usage: cpload.py [-h] [--urlfile URLFILE] [--fromuri FROMURI] [--touri TOURI] [--ratemin RATEMIN] [--ratemax RATEMAX] [--ramptime RAMPTIME] [--duration DURATION]

http log replay

optional arguments:
  -h, --help           show this help message and exit
  --urlfile URLFILE    the cleaned up http log
  --fromuri FROMURI    base uri/hostname as in the log file, this is used to edit it to the destination uri.
  --touri TOURI        the uri/hostname you want to send the traffic to
  --ratemin RATEMIN    minimum req/hour
  --ratemax RATEMAX    maximum req/hour
  --ramptime RAMPTIME  ramp up time in hours
  --duration DURATION  total test duration in hours
  --verbose            Output all request results.
  --filters FILTERS    the uri filters and rewrites file

example:

# verbose shows url with returned status code
python cpload.py --verbose --touri "http://127.0.0.1:8080" --ratemin 360 --ratemax 7200 --ramptime 0.1 --duration 0.2

# functional test
python cpload.py --urlfile logs.txt --fromuri base.uri --touri "https://tst.doesnotexist.org" --ratemin 360 --ratemax 7200 --ramptime 1 --duration 2

# stress test (real world rampup)
python cpload.py --urlfile logs.txt --fromuri base.uri --touri "https://tst.doesnotexist.org" --ratemin 360 --ratemax 7200 --ramptime 0.5  --duration 0.5 ;
python cpload.py --urlfile logs.txt --fromuri base.uri --touri "https://tst.doesnotexist.org" --ratemin 7200 --ratemax 90000 --ramptime 7 --duration 10

# Worst case
python cpload.py --urlfile logs.txt --fromuri base.uri --touri "https://tst.doesnotexist.org" --ratemin 360 --ratemax 7200 --ramptime 0.5  --duration 0.5 ;
python cpload.py --urlfile logs.txt --fromuri base.uri --touri "https://tst.doesnotexist.org" --ratemin 7200 --ratemax 120000 --ramptime 6 --duration 10
```

## Why is this cool

Well this script has some advantages over you normal apache bench, blaze meter, jmeter and gattling.io (which I all used before I made this script). 
It can tweak your urls and have a full conversation with your api backend if you'd like it to.
In a lot of scenarios there are a lot of steps that a typical user of you api takes while consuming your api that don't make sense to plainly replay.
These need a full on reenactment with session state, responses on actual data or dates replaced with always in the future dates.

This script has a few functions that can manipulate the urls from when you recorded them to how you want to replay them now.

## How to extend this tool

The real power is in the `filters.py` file, here you can add python code to edit the uri before the request is send out.

> example start with filters

```txt
python cpload.py --touri "http://192.168.1.2:8080" --ratemin 2000 --ratemax 40000 --ramptime 0.2 --duration 0.5 --filters filters.py
loaded filters:

 function      help text                   uri filter
------------  --------------------------  ------------
magic_edit    Select only magic products  /magic/
future        Always use future dates     api/v1/date
to_upper      URI TO UPPER                web
send_request  Send the get request 

Load test running at: 2000 req/hour
```

Filters can be loaded with `--filter your/path/filters.py`

Writing filters is not complicated, here is a to upper example:
```python
# ALL CAPS FOR ADDED DRAMA
def to_upper(http_pool, url):
    """URI TO UPPER"""
    send_request(http_pool, str(url).upper())
```

For more examples check out the filters.py and use it as a base to build your own.

## How fast is it

I tested it on my local LAN and got about 5 Mbit/s and 100% of 1 cpu core of an old i5 with the below command.
Starting another one added again 5Mbit/s, so I might be able to improve it by utilizing multiple cores better.
It is not the new LOIC but it does get the job done if you're doing sensible stuff.

```bash
python cpload.py --urlfile logs.txt --fromuri base.uri --touri "http://dockerbox:8080" --ratemin 100000000 --ratemax 1000000000
```

## How fast will this get

I'm doing some tweaking with multi core and reaching:

> 6500 req/s ( 23.400.000 req/h) @10Mbit/s egress from an old desktop i5

In the real world I haven't used it a lot over about 200.000 req/hour (55 req/s)

## why is it fast

It uses a connection pool to communicate with the target and keeps these connections open this reduces connection setup time for every request.
Usually you're not interested if the load balancer can handle the inrush of clients but the bottle neck is most of the time in the backend and that's what we test.

![Google Analytics](https://www.google-analytics.com/collect?v=1&tid=UA-48206675-1&cid=555&aip=1&t=event&ec=repo&ea=view&dp=gitlab%2Fcpload%2FREADME.md&dt=cpload)
